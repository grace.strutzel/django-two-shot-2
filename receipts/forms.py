from django import forms
from receipts.models import Account, ExpenseCategory, Receipt


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


class AccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]


class CategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]
